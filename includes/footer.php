<!-- footer -->
<section id="footer">
  <img src="/img/wave2.png" class="footer-img">
  <div class="container">
  <div class="container-fuild">
    <div class="row">
      <div class="footer_blog">
        <div class="row">
          <div class="col-md-6">
            <div class="main-heading left_text">
              <h2>Tentang Kami</h2>
            </div>
            <p>Tincidunt elit magnis nulla facilisis. Dolor sagittis maecenas. Sapien nunc amet ultrices, dolores sit ipsum velit purus aliquet, massa fringilla leo orci.</p>

          </div>
          <div class="col-md-6">
            <div class="main-heading left_text">
              <h2>Hubungi Kami</h2>
            </div>
            <p>Jalan.....<br>
              Pontianak, Indonesia<br>
              <span style="font-size:18px;"><a href="tel:+6286528333397">+6286528333397</a></span><br>
              yourmail@email.com</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  <hr>
  <p class="copyright">© Copyrights 2020</p>
  </div>
</div>
</section>

<!-- smooth scroll -->
<script src="/js/smooth-scroll.js"></script>
<script>
  var scroll = new SmoothScroll('a[href*="#"]');
</script>