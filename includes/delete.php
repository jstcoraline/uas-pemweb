<?php
// kita pakai $database yang ada di database_connect.php
require('../includes/database_connect.php');

$pesan_error = null;
$pesan_success = null;

if (!empty($_POST['no'])) {
 $no = (int)$_POST['no'];
 
 if (!preg_match("/^[0-9]+$/", $no)) {
  $pesan_error = "Nomor tidak valid";
 } else {
  try {
    $database->beginTransaction();
    
    $statement = $database->prepare(
      'DELETE FROM tb_' . $nama_tabel . ' WHERE No = :No'
    );
    
    $statement->execute([
      'No' => $no,
    ]);
    
    $database->commit();
    
    $pesan_success = "Berhasil menghapus data dengan id " . $no;
  } catch (Exception $e) {
    $pesan_error = "Tidak dapat menghapus data. " . $e->getMessage();
  }
 }
}
?>

<!DOCTYPE html>
<html>
  <?php require_once('../includes/header.php') ?>
  <body>
    <?php require_once('../includes/navbar.php') ?>
    <?php require_once('../includes/error.php') ?>
    <?php require_once('../includes/success.php') ?>
    <!-- main contents -->
    
    <div class="container mt-3">
      <div class="row">
        <div class="col">
          <div class="card w-50 mx-auto">
            <h5 align="center" class="card-header">Hapus Data <?php echo $judul ?></h5>
            <div class="card-body">
              <form action=<?php echo "/delete/". $nama_halaman . ".php"?> method="POST">
                <div class="form-group">
                  <label for="no">Hapus nomor
                  </label>
                  <input type="number" class="form-control" name="no" required>
                </div>
                <button type="submit" class="btn btn-danger">Hapus data
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    </section>
  <?php require_once('../includes/footer.php') ?>
  </body>
</html>

