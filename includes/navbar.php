<section id="nav-bar">
  <nav class="navbar navbar-expand-lg navbar-light">
    <a class="navbar-brand" href="#"><img src="/img/logo.png"> Data Kalimantan Barat</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fa fa-bars" aria-hidden="true"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Angka kemiskinan
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="/angkakemiskinan.php">Lihat</a>
            <a class="dropdown-item" href="/add/angkakemiskinan.php">Tambah</a>
            <a class="dropdown-item" href="/update/angkakemiskinan.php">Update</a>
            <a class="dropdown-item" href="/delete/angkakemiskinan.php">Delete</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Demografi
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="/demografi.php">Lihat</a>
            <a class="dropdown-item" href="/add/demografi.php">Tambah</a>
            <a class="dropdown-item" href="/update/demografi.php">Update</a>
            <a class="dropdown-item" href="/delete/demografi.php">Delete</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Indeks pembangunan
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="/indekspembangunan.php">Lihat</a>
            <a class="dropdown-item" href="/add/indekspembangunan.php">Tambah</a>
            <a class="dropdown-item" href="/update/indekspembangunan.php">Update</a>
            <a class="dropdown-item" href="/delete/indekspembangunan.php">Delete</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Angka pengangguran
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="/angkapengangguran.php">Lihat</a>
            <a class="dropdown-item" href="/add/angkapengangguran.php">Tambah</a>
            <a class="dropdown-item" href="/update/angkapengangguran.php">Update</a>
            <a class="dropdown-item" href="/delete/angkapengangguran.php">Delete</a>
          </div>
        </li>
      </ul>
    </div>
  </nav>		
</section>
    <!-- header -->
<div id="carousel-with-lb" class="carousel slide carousel-multi-item" data-ride="carousel">

  <ol class="carousel-indicators">
    <li data-target="#carousel-with-lb" data-slide-to="0" class="active secondary-color"></li>
    <li data-target="#carousel-with-lb" data-slide-to="1" class="secondary-color"></li>
    <li data-target="#carousel-with-lb" data-slide-to="2" class="secondary-color"></li>
    <li data-target="#carousel-with-lb" data-slide-to="3" class="secondary-color"></li>
    <li data-target="#carousel-with-lb" data-slide-to="4" class="secondary-color"></li>
  
  </ol>

  <div class="carousel-inner mdb-lightbox" role="listbox">
    <div id="mdb-lightbox-ui"></div>
    <!--Slide pertama-->
    <div class=" carousel-item active text-center">
      <figure class="col-md-2 d-md-inline-block">
          <img src="img/logo.png" class="img-fluid">
        </a>
      </figure>
      <figure class="col-md-2 d-md-inline-block">
          <img src="img/kab_ptk.jpg" class="img-fluid">
        </a>
      </figure>
      <figure class="col-md-2 d-md-inline-block">
      <img src="img/kab_sanggau.png" class="img-fluid">
        </a>
      </figure>
    </div>
    <!--/.Slide pertama-->

    <!--Slide kedua-->
    <div class="carousel-item text-center">
      <figure class="col-md-2 d-md-inline-block">
      <img src="img/landak.jpg" class="img-fluid">
        </a>
      </figure>
      <figure class="col-md-2 d-md-inline-block">
      <img src="img/melawi.jpg" class="img-fluid">
        </a>
      </figure>
      <figure class="col-md-2 d-md-inline-block">
      <img src="img/pontianak.jpg" class="img-fluid">
        </a>
        </figure>
    </div>

    <!--/.Slide kedua-->

    <!--Slide ketiga-->
    <div class="carousel-item text-center">
      <figure class="col-md-2 d-md-inline-block">
      <img src="img/sambas.png" class="img-fluid">
        </a>
      </figure>
      <figure class="col-md-2 d-md-inline-block">
          <img src="img/ketapang.png" class="img-fluid">
        </a>
      </figure>
      <figure class="col-md-2 d-md-inline-block">
          <img src="img/kubu_raya.jpg" class="img-fluid">
        </a>
        </figure>

    </div>
    <!--/.Slide ketiga-->

    <!--Slide keempat-->
    <div class="carousel-item text-center">
      <figure class="col-md-2 d-md-inline-block">
      <img src="img/kab_sintang.png" class="img-fluid">
        </a>
      </figure>
      <figure class="col-md-2 d-md-inline-block">
      <img src="img/kapuas_hulu.jpg" class="img-fluid">
        </a>
      </figure>
      <figure class="col-md-2 d-md-inline-block">
      <img src="img/kayong_utara.jpg" class="img-fluid">
        </a>
      </figure>
    </div>
    <!--/.Slide keempat-->
    <!--Slide kelima-->
    <div class="carousel-item text-center">
      <figure class="col-md-2 d-md-inline-block">
      <img src="img/singkawang.png" class="img-fluid">
        </a>
      </figure>
      <figure class="col-md-2 d-md-inline-block">
      <img src="img/kab_sekadau.png" class="img-fluid">
        </a>
      </figure>
      <figure class="col-md-2 d-md-inline-block">
          <img src="img/bengkayang.jpg" class="img-fluid">
        </a>
      </figure>
    </div>
    <!--/.Slide kelima-->

  </div>
</div>
  <!-- end header -->