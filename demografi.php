<?php
// kita pakai $database yang ada di database_connect.php
require('includes/database_connect.php');

// spesifikasi query untuk tabel demografi penduduk
$query_tabel_demografipenduduk = 'SELECT * FROM tb_demografipenduduk ORDER BY No ASC';

// siapkan statement dengan query yang udah ada
$statement = $database->prepare($query_tabel_demografipenduduk);

// jalankan statement dan simpan hasilnya di hasil_query_demografipenduduk
$statement->execute();

// hasil query ini akan digunakan nanti
$hasil_query_demografipenduduk = $statement->fetchAll();

// supaya statement yang lain dapat dieksekusi, 
// statement sebelumnya harus diclose terlebih dahulu
$statement->closeCursor()
?>

<!DOCTYPE html>
<html>
<?php require_once('includes/header.php') ?>
<body>
	<?php require_once('includes/navbar.php') ?>
  
	<!-- main contents -->
	<section id="contents">
		<h3 align="center">Tabel Demografi</h3> 
		<div class="container">
			<table class="table table-hover">
				<thead>
				<tr>
					<th>No</th>
					<th>ID kelompok</th>
					<th>Nama kabupaten</th>
					<th>Tahun</th>
					<th>Jumlah laki-laki</th>
					<th>Jumlah perepmpuan</th>
					<th>Jumlah total</th>
          <th>Laju pertumbuhan (%)</th>
          <th>Luas wilayah (km^2)</th>
          <th>Kepadatan</th>
          <th>Sumber data</th>
					</thead>
				</tr>
				<tbody>
				<?php foreach($hasil_query_demografipenduduk as $baris) : ?>
				<tr>
					<!-- sesuai nama yang ada di database -->
					<td><?php echo $baris['No']; ?></td>
					<td><?php echo $baris['id_kel']; ?></td>
					<td><?php echo $baris['nama_kab']; ?></td>
					<td><?php echo $baris['tahun']; ?></td>
					<td><?php echo $baris['Laki_Laki']; ?></td>
					<td><?php echo $baris['Perempuan']; ?></td>
          <td><?php echo $baris['Laki_Laki']+$baris['Perempuan']; ?></td>
          <td><?php echo $baris['Laju_Pertumbuhan'] ?></td>
          <td><?php echo $baris['Luas_Wilayah'] ?></td>
          <td><?php echo $baris['Kepadatan'] ?></td>
          <td><?php echo $baris['sumber_data'] ?></td>
				</tr>
				<?php endforeach; ?>
				</tbody>
			</table>			
		</div>
	</section>
	<?php require_once('includes/footer.php') ?>
</body>
</html>
