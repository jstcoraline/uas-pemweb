<?php
// kita pakai $database yang ada di database_connect.php
require('../includes/database_connect.php');

$pesan_error = null;
$pesan_success = null;
$baris = null;

if (!empty($_POST['update_get'])) {
  // spesifikasi query untuk tabel pengangguran
  $query_tabel_ipm = 'SELECT * FROM tb_ipm WHERE No = :No';

  // siapkan statement dengan query yang udah ada
  $statement = $database->prepare($query_tabel_ipm);

  // jalankan statement
  $statement->execute([
    'No' => (int)$_POST['update_get'],
  ]);

  // hasil query ini akan digunakan nanti
  $baris = $statement->fetch(PDO::FETCH_ASSOC);

  // supaya statement yang lain dapat dieksekusi, 
  // statement sebelumnya harus diclose terlebih dahulu
  $statement->closeCursor();
} else {
  // Ambil data dari form yang diisi.
  $no = filter_input(INPUT_POST, "no");
  $id_kelompok = filter_input(INPUT_POST, "id_kel");
  $nama_kabupaten = filter_input(INPUT_POST, "nama_kab");
  $tahun = filter_input(INPUT_POST, "tahun");
  $ipm = filter_input(INPUT_POST, "ipm");
  $sumber_data = filter_input(INPUT_POST, "sumber_data");

  $harus_diisi = array('no', 'id_kel', 'nama_kab', 'tahun', 'ipm', 'sumber_data');

  // Hitung jumlah bagian yang terisi (tidak kosong)
  $jumlah_terisi = 0;
  foreach ($harus_diisi as $bagian) {
     if (!empty($_POST[$bagian])) {
       $jumlah_terisi++;
     }
  }
  
  if ($jumlah_terisi == count($harus_diisi)) { // Semua bagian terisi
    // validasi untuk setiap bagian yang diisi
    if (!preg_match("/^[0-9]+$/", $no)) {
      $pesan_error = "Nomor tidak valid";
    } else if (!preg_match("/^[0-9]{1,3}$/", $id_kelompok)) {
      $pesan_error = "ID Kelompok tidak valid";
    } else if (!preg_match("/^[a-zA-Z ]{3,30}$/", $nama_kabupaten)) {
      $pesan_error = "Nama kabupaten tidak valid";
    } else if (!preg_match("/^[0-9]+$/", $tahun)) {
      $pesan_error = "Tahun tidak valid";
    } else if (!preg_match("/^[0-9]+\.?[0-9]*$/", $ipm)) {
      $pesan_error = "IPM tidak valid";
    } else if (!preg_match("/^.{1,255}$/", $sumber_data)) {
      $pesan_error = "Sumber data tidak valid";
    } else {
      try {
        $database->beginTransaction();
        
        $statement = $database->prepare(
          'UPDATE `tb_ipm` SET id_kel = :id_kel, nama_kab = :nama_kab, tahun = :tahun, Indeks_Pembangunan_Manusia = :Indeks_Pembangunan_Manusia, sumber_data = :sumber_data WHERE No = :No'
        );
        
        $statement->execute([
          'No' => $no,
          'id_kel' => $id_kelompok,
          'nama_kab' => $nama_kabupaten,
          'tahun' => $tahun,
          'Indeks_Pembangunan_Manusia' => $ipm,
          'sumber_data' => $sumber_data,
        ]);
        
        $database->commit();
        
        $pesan_success = "Berhasil update data dengan id " . $no;
      } catch (Exception $e) {
        $pesan_error = "Tidak dapat update data. " . $e->getMessage();
      }
    }
  } else if ($jumlah_terisi > 0) {
    $pesan_error = "Isi semua bagian form " . $jumlah_terisi;
  } else {
    $pesan_error = 'Masukkan nomor data untuk melakukan update';
  }
}
?>

<!DOCTYPE html>
<html>
  <?php require_once('../includes/header.php') ?>
  <body>
    <?php require_once('../includes/navbar.php') ?>
    <?php require_once('../includes/error.php') ?>
    <?php require_once('../includes/success.php') ?>
    <!-- main contents -->
    <div class="container mt-3">
      <div class="row">
        <div class="col">
          <div class="card w-50 mx-auto">
            <h5 align="center" class="card-header">Nomor Update Angka Pengangguran
            </h5>
            <div class="card-body">
              <form action="/update/indekspembangunan.php" method="POST">
                <div class="form-group">
                  <label for="update_get">Update nomor
                  </label>
                  <input type="number" class="form-control" name="update_get" required>
                </div>
                <button type="submit" class="btn btn-primary">Dapatkan data</button>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <div class="card w-50 mx-auto">
            <h5 align="center" class="card-header">Update Data Angka Pengangguran
            </h5>
            <div class="card-body">
              <form action="/update/indekspembangunan.php" method="POST">
                <div class="form-group">
                  <label for="no">Update nomor
                  </label>
                  <?php 
                    if ($baris != null) {
                      echo '<input type="number" class="form-control" name="no" value="' . $baris['No'] . '" required>';
                    } else {
                      echo '<input type="number" class="form-control" name="no" required>';
                    }                      
                  ?>
                </div>
                <div class="form-group">
                  <label for="id_kel">ID kelompok
                  </label>
                  <?php 
                    if ($baris != null) {
                      echo '<input type="number" class="form-control" name="id_kel" value="' . $baris['id_kel'] . '" required>';
                    } else {
                      echo '<input type="number" class="form-control" name="id_kel" required>';
                    }                      
                  ?>
                </div>
                <div class="form-group">
                  <label for="nama_kab">Nama kabupaten
                  </label>
                  <?php 
                    if ($baris != null) {
                      echo '<input type="text" class="form-control" name="nama_kab" value="' . $baris['nama_kab'] . '" required>';
                    } else {
                      echo '<input type="text" class="form-control" name="nama_kab" required>';
                    }                      
                  ?>
                </div>
                <div class="form-group">
                  <label for="tahun">Tahun
                  </label>
                  <?php 
                    if ($baris != null) {
                      echo '<input type="number" class="form-control" name="tahun" value="' . $baris['tahun'] . '" required>';
                    } else {
                      echo '<input type="number" class="form-control" name="tahun" required>';
                    }                      
                  ?>
                </div>
                <div class="form-group">
                  <label for="ipm">IPM
                  </label>
                  <?php 
                    if ($baris != null) {
                      echo '<input type="number" class="form-control" step=0.01 min=0.00 name="ipm" value="' . number_format($baris['Indeks_Pembangunan_Manusia'], 2) . '" required>';
                    } else {
                      echo '<input type="number" class="form-control" step=0.01 min=0.00 name="ipm" required>';
                    }                      
                  ?>
                </div>
                <div class="form-group">
                  <label for="sumber_data">Sumber data
                  </label>
                  <?php 
                    if ($baris != null) {
                      echo '<input type="text" class="form-control" name="sumber_data" value="' . $baris['sumber_data'] . '" required>';
                    } else {
                      echo '<input type="text" class="form-control" name="sumber_data" required>';
                    }                      
                  ?>
                </div>
                <button type="submit" class="btn btn-primary">Update data
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    </section>
  <?php require_once('../includes/footer.php') ?>
  </body>
</html>

