<?php
// kita pakai $database yang ada di database_connect.php
require('includes/database_connect.php');

// spesifikasi query untuk tabel angka pengangguran
$query_tabel_pengangguran = 'SELECT * FROM tb_pengangguran ORDER BY No ASC';

// siapkan statement dengan query yang udah ada
$statement = $database->prepare($query_tabel_pengangguran);

// jalankan statement dan simpan hasilnya di hasil_query_ipm
$statement->execute();

// hasil query ini akan digunakan nanti
$hasil_query_pengangguran = $statement->fetchAll();

// supaya statement yang lain dapat dieksekusi, 
// statement sebelumnya harus diclose terlebih dahulu
$statement->closeCursor()
?>

<!DOCTYPE html>
<html>
<?php require_once('includes/header.php') ?>
<body>
	<?php require_once('includes/navbar.php') ?>
  
	<!-- main contents -->
	<section id="contents">
		<h3 align="center">Tabel Angka Pengangguran</h3> 
		<div class="container">
			<table class="table table-hover">
				<thead>
				<tr>
					<th>No</th>
					<th>ID kelompok</th>
					<th>Nama kabupaten</th>
					<th>Tahun</th>
					<th>Angka pengangguran</th>
          <th>Sumber data</th>
					</thead>
				</tr>
				<tbody>
				<?php foreach($hasil_query_pengangguran as $baris) : ?>
				<tr>
					<!-- sesuai nama yang ada di database -->
					<td><?php echo $baris['No']; ?></td>
					<td><?php echo $baris['id_kel']; ?></td>
					<td><?php echo $baris['nama_kab']; ?></td>
					<td><?php echo $baris['tahun']; ?></td>
					<td><?php echo $baris['Angka_Pengangguran']; ?></td>
          <td><?php echo $baris['sumber_data'] ?></td>
				</tr>
				<?php endforeach; ?>
				</tbody>
			</table>			
		</div>
	</section>
	<?php require_once('includes/footer.php') ?>
</body>
</html>
