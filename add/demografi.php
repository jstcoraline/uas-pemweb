<?php
// kita pakai $database yang ada di database_connect.php
require('../includes/database_connect.php');

// Ambil data dari form yang diisi.
$id_kelompok = filter_input(INPUT_POST, "id_kel");
$nama_kabupaten = filter_input(INPUT_POST, "nama_kab");
$tahun = filter_input(INPUT_POST, "tahun");
$laki_laki = filter_input(INPUT_POST, "laki_laki");
$perempuan = filter_input(INPUT_POST, "perempuan");
$jumlah_penduduk = $laki_laki + $perempuan;
$laju_pertumbuhan = filter_input(INPUT_POST, "laju_pertumbuhan");
$luas_wilayah = filter_input(INPUT_POST, "luas_wilayah");
$kepadatan = filter_input(INPUT_POST, "kepadatan");
$sumber_data = filter_input(INPUT_POST, "sumber_data");

$harus_diisi = array('id_kel', 'nama_kab', 'tahun', 'laki_laki', 'perempuan', 'laju_pertumbuhan', 'luas_wilayah', 'kepadatan', 'sumber_data');

// Hitung jumlah bagian yang terisi (tidak kosong)
$jumlah_terisi = 0;
foreach ($harus_diisi as $bagian) {
   if (!empty($_POST[$bagian])) {
     $jumlah_terisi++;
   }
}

$pesan_error = null;
$pesan_success = null;
if ($jumlah_terisi == count($harus_diisi)) { // Semua bagian terisi
  // validasi untuk setiap bagian yang diisi
  if (!preg_match("/^[0-9]{1,3}$/", $id_kelompok)) {
    $pesan_error = "ID Kelompok tidak valid";
  } else if (!preg_match("/^[a-zA-Z ]{3,30}$/", $nama_kabupaten)) {
    $pesan_error = "Nama kabupaten tidak valid";
  } else if (!preg_match("/^[0-9]+$/", $tahun)) {
    $pesan_error = "Tahun tidak valid";
  } else if (!preg_match("/^[0-9]+$/", $laki_laki)) {
    $pesan_error = "Jumlah penduduk (laki-laki) tidak valid";
  } else if (!preg_match("/^[0-9]+$/", $perempuan)) {
    $pesan_error = "Jumlah penduduk (perempuan) tidak valid";
  } else if (!preg_match("/^[0-9]+\.?[0-9]*$/", $laju_pertumbuhan)) {
    $pesan_error = "Laju pertumbuhan tidak valid";
  } else if (!preg_match("/^[0-9]+\.?[0-9]*$/", $luas_wilayah)) {
    $pesan_error = "Luas wilayah tidak valid";
  } else if (!preg_match("/^[0-9]+\.?[0-9]*$/", $kepadatan)) {
    $pesan_error = "Kepadatan tidak valid";
  } else if (!preg_match("/^.{1,255}$/", $sumber_data)) {
    $pesan_error = "Sumber data tidak valid";
  } else {
    $query_max_id = $database->query('SELECT MAX(No) AS `maxid` FROM `tb_demografipenduduk`');
    $new_id = 1 + (int)$query_max_id->fetchColumn();
    $query_max_id->closeCursor();

    try {
      $database->beginTransaction();
      
      $statement = $database->prepare(
        'INSERT INTO `tb_demografipenduduk` (`No`, `id_kel`, `nama_kab`, `tahun`, `Laki_Laki`, `Perempuan`, `Jumlah_Penduduk`, `Laju_Pertumbuhan`, `Luas_Wilayah`, `Kepadatan`, `sumber_data`) VALUES (:No, :id_kel, :nama_kab, :tahun, :Laki_Laki, :Perempuan, :Jumlah_Penduduk, :Laju_Pertumbuhan, :Luas_Wilayah, :Kepadatan, :sumber_data)'
      );
      
      $statement->execute([
        'No' => $new_id,
        'id_kel' => $id_kelompok,
        'nama_kab' => $nama_kabupaten,
        'tahun' => $tahun,
        'Laki_Laki' => $laki_laki,
        'Perempuan' => $perempuan,
        'Jumlah_Penduduk' => $jumlah_penduduk,
        'Laju_Pertumbuhan' => $laju_pertumbuhan,
        'Luas_Wilayah' => $luas_wilayah,
        'Kepadatan' => $kepadatan,
        'sumber_data' => $sumber_data,
      ]);
      
      $database->commit();
      
      $pesan_success = "Berhasil menambah data dengan id " . $new_id;
    } catch (Exception $e) {
      $pesan_error = "Tidak dapat menambah data. " . $e->getMessage();
    }
  }
} else if ($jumlah_terisi > 0) {
  $pesan_error = "Isi semua bagian form";
}
?>

<!DOCTYPE html>
<html>
  <?php require_once('../includes/header.php') ?>
  <body>
    <?php require_once('../includes/navbar.php') ?>
    <?php require_once('../includes/error.php') ?>
    <?php require_once('../includes/success.php') ?>
    <!-- main contents -->
    <div class="container mt-3">
      <div class="row">
        <div class="col">
          <div class="card w-50 mx-auto">
            <h5 align="center" class="card-header">Tambah Data Demografi
            </h5>
            <div class="card-body">
              <form action="/add/demografi.php" method="POST">
                <div class="form-group">
                  <label for="id_kel">ID kelompok
                  </label>
                  <input type="number" class="form-control" name="id_kel" required>
                </div>
                <div class="form-group">
                  <label for="nama_kab">Nama kabupaten
                  </label>
                  <input type="text" class="form-control" name="nama_kab" required>
                </div>
                <div class="form-group">
                  <label for="tahun">Tahun
                  </label>
                  <input type="number" class="form-control" name="tahun" required>
                </div>
                <div class="form-group">
                  <label for="laki_laki">Jumlah laki-laki
                  </label>
                  <input type="number" class="form-control" name="laki_laki" required>
                </div>
                <div class="form-group">
                  <label for="perempuan">Jumlah perempuan
                  </label>
                  <input type="number" class="form-control" name="perempuan" required>
                </div>
                <div class="form-group">
                  <label for="laju_pertumbuhan">Laju pertumbuhan
                  </label>
                  <input type="number" class="form-control" step=0.01 min=0.00 name="laju_pertumbuhan" required>
                </div>
                <div class="form-group">
                  <label for="luas_wilayah">Luas wilayah
                  </label>
                  <input type="number" class="form-control" step=0.01 min=0.00 name="luas_wilayah" required>
                </div>
                <div class="form-group">
                  <label for="kepadatan">Kepadatan
                  </label>
                  <input type="number" class="form-control" name="kepadatan" required>
                </div>
                <div class="form-group">
                  <label for="sumber_data">Sumber data
                  </label>
                  <input type="text" class="form-control" name="sumber_data" required>
                </div>
                <button type="submit" class="btn btn-primary">Tambah data</button>
                <button type="reset" class="btn btn-danger">Kosongkan</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    </section>
  <?php require_once('../includes/footer.php') ?>
  </body>
</html>
