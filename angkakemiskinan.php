<?php
// kita pakai $database yang ada di database_connect.php
require('includes/database_connect.php');

$batas = 20;
$halaman = isset($_GET['halaman'])?(int)$_GET['halaman'] : 1;
$halaman_awal = ($halaman>1) ? ($halaman * $batas) - $batas : 0;	
$previous = $halaman - 1;
$next = $halaman + 1;

// spesifikasi query untuk tabel angka miskin
$query_tabel_angkamiskin = 'SELECT * FROM tb_angkamiskin ORDER BY No ASC LIMIT'.$halaman_awal.",".$batas;

// siapkan statement dengan query yang udah ada
$statement = $database->prepare($query_tabel_angkamiskin);

// jalankan statement dan simpan hasilnya di hasil_query_angkamiskin
$statement->execute();

// hitung data
$jumlah_data = $statement->rowCount();
$total_halaman = ceil($jumlah_data / $batas);

// hasil query ini akan digunakan nanti
$hasil_query_angkamiskin = $statement->fetchAll();

// supaya statement yang lain dapat dieksekusi, 
// statement sebelumnya harus diclose terlebih dahulu
$statement->closeCursor();
?>

<!DOCTYPE html>
<html>
<?php require_once('includes/header.php') ?>
<body>
	<?php require_once('includes/navbar.php') ?>
  
	<!-- main contents -->
	<section id="contents">
		<h3 align="center">Tabel Angka Miskin</h3> 
		<div class="container">
			<table class="table table-hover">
				<thead>
				<tr>
					<th>No</th>
					<th>ID kelompok</th>
					<th>Kabupaten</th>
					<th>Tahun</th>
					<th>Angka kemiskinan</th>
					<th>Sumber data</th>
					</thead>
				</tr>
				<tbody>
				<?php 
				foreach ($hasil_query_angkamiskin as $baris):
				?> 
				<tr>
					<!-- sesuai nama yang ada di database (No, id_kel, nama_kab, ...) -->
					<td><?php echo $nomor++; ?></td>
					<td><?php echo $baris['id_kel']; ?></td>
					<td><?php echo $baris['nama_kab']; ?></td>
					<td><?php echo $baris['tahun']; ?></td>
					<td><?php echo $baris['Angka_Kemiskinan']; ?></td>
					<td><?php echo $baris['sumber_data']; ?></td>
				</tr>
				<?php endforeach; ?>
				</tbody>
			</table>		
			<nav>
			<ul class="pagination justify-content-center">
				<li class="page-item">
					<a class="page-link" <?php if($halaman > 1){ echo "href=angkakemiskinan.php'?halaman=$Previous'";} ?>>Previous</a>
				</li>
				<?php 
				for($x=1;$x<=$total_halaman;$x++){
					?> 
					<li class="page-item"><a class="page-link" href="angkakemiskinan.php ?halaman=<?php echo $x ?>"><?php echo $x; ?></a></li>
					<?php
				}
				?>				
				<li class="page-item">
					<a  class="page-link" <?php if($halaman < $total_halaman) { echo "href=angkakemiskinan.php'?halaman=$next'"; } ?>>Next</a>
				</li>
			</ul>
		</nav>
		</div>
	</section>
	<?php require_once('includes/footer.php') ?>
</body>
</html>
